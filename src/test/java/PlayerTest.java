import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class PlayerTest {

    @Test
    public void shouldReturnCooperativeMoveType(){
        PlayerBehaviour consoleBehaviour = new ConsoleBehavior(new Scanner("1"));
        Player player = new Player(consoleBehaviour);
        Assert.assertEquals(MoveType.COOPERATE, player.move());
    }

    @Test
    public void shouldReturnCheatMoveType(){
        PlayerBehaviour consoleBehaviour = new ConsoleBehavior(new Scanner("2"));
        Player player = new Player(consoleBehaviour);
        Assert.assertEquals(MoveType.CHEAT, player.move());
    }

    @Test
    public void shouldReturnInvalidMoveType(){

        PlayerBehaviour consoleBehaviour = new ConsoleBehavior(new Scanner("3"));
        Player player = new Player(consoleBehaviour);
        Assert.assertEquals(MoveType.INVALID_MOVE, player.move());
    }

    @Test
    public void shouldUpdateCumulativeScoreByOne(){
        PlayerBehaviour consoleBehaviour = new ConsoleBehavior(new Scanner("1"));
        Player player = new Player(consoleBehaviour);
        Assert.assertEquals(2, player.addScore(2));
    }
}
