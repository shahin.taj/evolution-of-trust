import java.util.Scanner;

public class ConsoleBehavior implements PlayerBehaviour{

    private Scanner scanner;

    public ConsoleBehavior(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public MoveType move() {
        return MoveType.getMoveType(Integer.parseInt(this.scanner.next()));
    }
}
