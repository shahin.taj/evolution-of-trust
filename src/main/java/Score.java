import java.util.Objects;

public class Score {


    private int playerTwoScore;
    private int playerOneScore;

    Score(int playerOneScore, int playerTwoScore)
    {
        this.playerOneScore = playerOneScore;
        this.playerTwoScore = playerTwoScore;
    }

    int getPlayerTwoScore() {
        return playerTwoScore;
    }

    int getPlayerOneScore() {
        return playerOneScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score = (Score) o;
        return playerTwoScore == score.playerTwoScore &&
                playerOneScore == score.playerOneScore;
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerTwoScore, playerOneScore);
    }

    @Override
    public String toString() {
        return "Score{" +
                "playerTwoScore=" + playerTwoScore +
                ", playerOneScore=" + playerOneScore +
                '}';
    }
}
